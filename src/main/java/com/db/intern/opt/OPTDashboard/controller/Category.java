package com.db.intern.opt.OPTDashboard.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


public enum Category {
	UBR_LEVEL_2("UBR Level 2", "UBR_Group"),
	UBR_LEVEL_1("UBR Level 1", "UBR_Group"),
	SUB_BU("Sub BU", "UBR_Group"),
	COUNTRY_DESC("Country Desc","Location_Group"),
	LOCATION("Location","Location_Group");
	
	private String label;
	private String group;
	
	Category(String label, String group) {
		this.label = label;
		this.group = group;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	
	static public void setLevels(){
		UBR_LEVELS.add(UBR_LEVEL_2);
		UBR_LEVELS.add(UBR_LEVEL_1);
		UBR_LEVELS.add(SUB_BU);
	
		LOCATION_LEVELS.add(COUNTRY_DESC);
		LOCATION_LEVELS.add(LOCATION);
		
		GROUPS.put("UBR_Group", UBR_LEVELS);
		GROUPS.put("Location_Group", LOCATION_LEVELS);
	}
	
	public static Category getEnumByLabel(String label){
        for(Category c : Category.values()){
            if(c.label.equals(label)) return c;
        }
        return null;
    }

	private final static List<Category> UBR_LEVELS = new ArrayList<>();
	private final static List<Category> LOCATION_LEVELS = new ArrayList<>();	
	final static ConcurrentHashMap<String, List<Category>> GROUPS = new ConcurrentHashMap<String, List<Category>>();	
	
}
