package com.db.intern.opt.OPTDashboard.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.db.intern.opt.OPTDashboard.model.Filter;

@Repository
public interface FilterRepository extends JpaRepository<Filter, Long>{

}
