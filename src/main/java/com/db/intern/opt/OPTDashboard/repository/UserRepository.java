package com.db.intern.opt.OPTDashboard.repository;

import org.springframework.stereotype.Repository;

import com.db.intern.opt.OPTDashboard.model.User;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	
	User findByemail(String email);
	User findByname(String name);
	User findByisLoggedInTrue();
	
}