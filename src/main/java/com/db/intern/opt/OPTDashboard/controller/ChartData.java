package com.db.intern.opt.OPTDashboard.controller;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javafx.util.Pair;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.db.intern.opt.OPTDashboard.controller.CacheManager.Cache;
import com.db.intern.opt.OPTDashboard.model.JobData;

public class ChartData {
	
	public static ConcurrentHashMap<String,Integer> getBarChartData(String category){
		ConcurrentHashMap<String, Integer> barChartData= new ConcurrentHashMap<String, Integer>();
		List<JobData> jobDataList = CacheManager.getCache(Cache.JOB_DATA);
		for(JobData row:jobDataList) {
			String categoryType=row.getAttribute(category);
			int count = barChartData.containsKey(categoryType) ? barChartData.get(categoryType) : 0;
			barChartData.put(categoryType, count + 1);
		}
		return barChartData;
	}
	
	 // cat1- x axis and cat2- division/stacks example- cat1=subBU and cat2=positionIDStatus
	public static ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> getStackedBarChartData(String category1,String category2){
		ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> stackedBarChartData= new ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>>();
		List<JobData> jobDataList = CacheManager.getCache(Cache.JOB_DATA);
		for(JobData row:jobDataList) {
			String category1Type=row.getAttribute(category1);
			String category2Type=row.getAttribute(category2);
			if (stackedBarChartData.containsKey(category1Type)){
				
				int count = stackedBarChartData.get(category1Type).containsKey(category2Type) ? stackedBarChartData.get(category1Type).get(category2Type) : 0;
				stackedBarChartData.get(category1Type).put(category2Type, count + 1);
				
	         }else{
	        	 ConcurrentHashMap<String,Integer> InnerMap = new ConcurrentHashMap<String, Integer>();
	        	 InnerMap.put(category2Type,1);
	        	 stackedBarChartData.put(category1Type,InnerMap);
	         }
		}
		return stackedBarChartData;
	}
	
	public static Set<Pair<String,String>> getStackedBarChartDataFilters(String category1,String category2){
		Set<Pair<String,String>> stackedBarChartDataFilters = new HashSet<Pair<String,String>>();
		List<JobData> jobDataList = CacheManager.getCache(Cache.JOB_DATA);
		for(JobData row:jobDataList) {
			String category1Type=row.getAttribute(category1);
			String category2Type=row.getAttribute(category2);
			Pair<String,String> p1 = new Pair <String, String> (category1, category1Type);
			Pair<String,String> p2 = new Pair <String, String> (category2, category2Type);
			stackedBarChartDataFilters.add(p1);
			stackedBarChartDataFilters.add(p2);
		}
		return stackedBarChartDataFilters;
	}
	
	public static ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> filter(String category1,String category2,List<String> values){
		ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> stackedBarChartData = getStackedBarChartData(category1,category2);
		ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> filteredStackedBarChartData = new ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>>();
		stackedBarChartData.forEach((cat1, innermap)->{
			if(!values.contains(cat1)) return;
			Map<String, Integer> filteredInnerMap = innermap.entrySet().stream()
			                   .filter(i -> values.contains(i.getKey()))
			                   .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
			filteredStackedBarChartData.put(cat1, new ConcurrentHashMap<String,Integer> (filteredInnerMap));
		});
		
		return filteredStackedBarChartData;
		
	}
	
	public static ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> getBannerData(){
		ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> BannerData= new ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>>();
		List <String> category = Arrays.asList("positionIDStatus", "genderDesc", "pwd");
		for(String categoryIterator: category) {
			BannerData.put(categoryIterator,getBarChartData(categoryIterator));
		}
		return BannerData;
	}
}




