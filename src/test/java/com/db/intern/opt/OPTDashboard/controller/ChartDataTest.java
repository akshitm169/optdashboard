package com.db.intern.opt.OPTDashboard.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.junit.jupiter.api.Test;

import com.db.intern.opt.OPTDashboard.controller.CacheManager.Cache;
import com.db.intern.opt.OPTDashboard.model.JobData;

import javafx.util.Pair;

class ChartDataTest {

	private static final String CSV_PATH = "src/main/resources/JobData.csv";
	
	@Test
	void init() {
		List<JobData> jobDataList= CSVReader.readDataFromCSV(CSV_PATH);
		CacheManager.setCache(Cache.JOB_DATA,jobDataList);
	}
	
	@Test
	void testGetBarChartData() {
		ConcurrentHashMap<String, Integer> result =ChartData.getBarChartData("positionIDStatus");
		ConcurrentHashMap<String, Integer> expected= new ConcurrentHashMap<String, Integer>();
		expected.put("Open - Approved", 54);
		expected.put("On Hold", 53);
		expected.put("Offered", 54);
		assertEquals(expected,result);
	}
	
	@Test
	void testGetStackedBarChartData() {
		ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> result =ChartData.getStackedBarChartData("UBR Level 2","positionIDStatus");
		ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> expected= new ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>>();
		ConcurrentHashMap<String,Integer> temp1 = new ConcurrentHashMap<String, Integer>();
		temp1.put("Open - Approved", 22);
		temp1.put("On Hold", 22);
		temp1.put("Offered", 27);
		expected.put("Level_2_Infrastructure", temp1);
		ConcurrentHashMap<String,Integer> temp2 = new ConcurrentHashMap<String, Integer>();
		temp2.put("Open - Approved", 32);
		temp2.put("On Hold", 31);
		temp2.put("Offered", 27);
		expected.put("Level_2_Banking", temp2);
		assertEquals(expected,result);

	}
	
	
	@Test
	void testGetStackedBarChartDataFilters() {
		Set<Pair<String,String>> result = ChartData.getStackedBarChartDataFilters("UBR Level 2","positionIDStatus");
		Set<Pair<String,String>> expected = new HashSet<Pair<String,String>>();
		expected.add(new Pair("UBR Level 2","Level_2_Banking"));
		expected.add(new Pair("UBR Level 2","Level_2_Infrastructure"));
		expected.add(new Pair("positionIDStatus","Open - Approved"));
		expected.add(new Pair("positionIDStatus","Offered"));
		expected.add(new Pair("positionIDStatus","On Hold"));
		assertEquals(expected,result);
	}
	
	@Test
	void testFilter() {
		ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> result =  ChartData.filter("UBR Level 2","positionIDStatus", Arrays.asList("Level_2_Banking","On Hold","Offered"));
		ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> expected= new ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>>();
		ConcurrentHashMap<String,Integer> temp1 = new ConcurrentHashMap<String, Integer>();
		temp1.put("On Hold", 31);
		temp1.put("Offered", 27);
		expected.put("Level_2_Banking", temp1);
		assertEquals(expected,result);
	}
	
	@Test
	void testGetBannerData() {
		ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> result =  ChartData.getBannerData();
		ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> expected= new ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>>();
		ConcurrentHashMap<String,Integer> temp1 = new ConcurrentHashMap<String, Integer>();
		temp1.put("Open - Approved", 54);
		temp1.put("On Hold", 53);
		temp1.put("Offered", 54);
		expected.put("positionIDStatus", temp1);
		ConcurrentHashMap<String,Integer> temp2 = new ConcurrentHashMap<String, Integer>();
		temp2.put("TRUE", 6);
		temp2.put("FALSE", 155);
		expected.put("pwd", temp2);
		ConcurrentHashMap<String,Integer> temp3 = new ConcurrentHashMap<String, Integer>();
		temp3.put("Female", 74);
		temp3.put("Male", 87);
		expected.put("genderDesc", temp3);
		assertEquals(expected,result);
	}
	

}
