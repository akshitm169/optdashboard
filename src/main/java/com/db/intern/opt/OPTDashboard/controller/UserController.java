package com.db.intern.opt.OPTDashboard.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.db.intern.opt.OPTDashboard.model.Chart;
import com.db.intern.opt.OPTDashboard.model.Dashboard;
import com.db.intern.opt.OPTDashboard.model.Filter;
import com.db.intern.opt.OPTDashboard.model.User;
import com.db.intern.opt.OPTDashboard.repository.UserRepository;

@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping("/api/users/")
public class UserController {
	
	@Autowired
	UserFunctions userFunctions;
	@Autowired
	UserRepository userRepository;
	
	// user will take 3 things- email, name, password
	@PostMapping("/register")
	public String register(@RequestBody User user){
//		System.out.println(user);
//		System.out.println("hi");
		return userFunctions.registerUtil(user);
	}
	
	//
	@PostMapping("/login")
	public User login(@RequestBody User user){
//		System.out.println(user);
		return userFunctions.loginUtil(user);
	}
	
	@GetMapping("/initialize")
	public User initialize(){
		return userFunctions.initalizeUtil();
	}
	
	@PostMapping("/logout")
	public String logout(){
		return userFunctions.logoutUtil();
	}
	
	// Dashboard will take 2 things- isDefault and name
	@PostMapping("/addDashboard")
	public Dashboard addDashboard(@RequestBody Dashboard dashboard){
		return userFunctions.addDashboardUtil(dashboard);
	}
	
	@GetMapping("/getListOfDashboard")
	public List<Dashboard> getListOfDashboard(){
		return userFunctions.getListOfDashboardUtil();
	}
	@GetMapping("/hasDashboards")
	public Boolean hasDashboards(){
		return !(userFunctions.getListOfDashboardUtil().size()==0);
	}
	@GetMapping("/getDefaultDashboard")
	public Dashboard getDefaultDashboard(){
		return userFunctions.getDefaultDashboardUtil();
	}
	@GetMapping("/getDashboard")
	public Dashboard getDashboard(@RequestParam long id){
		return userFunctions.getDashboardUtil(id);
	}
	// chart will take name, type, category_1, category_2, aggregation
	@PostMapping("/addChart")
	public String addChart(@RequestParam long id, @RequestBody Chart chart){
		if(chart.getType().equals("Stacked Bar Chart")||chart.getType().equals("Bar Chart"))
			return userFunctions.addStackedChartUtil(id,chart);
		else if(chart.getType().equals("YTD Chart"))
			return  userFunctions.addYTDChartUtil(id,chart);
		else return null;
	}
	
	@GetMapping("/getListOfCharts")
	public List<Chart> getListOfCharts(@RequestParam long id){
		return userFunctions.getListOfChartsUtil(id);
	}
	@GetMapping("/getChartFilters")
	public List<String> getChartFilters(@RequestParam long id){
		return userFunctions.getChartFiltersUtil(id);
	}
	
	@PostMapping("/updateStackChartCategory1")
	public String updateStackChartCategory1(@RequestParam long id,@RequestParam String Category1){
		return userFunctions.updateStackChartCategory1Util(id,Category1);
	}
	
	@PostMapping("/updateStackChartFilters")
	public String updateStackChartFilters(@RequestParam long id, @RequestBody List<Filter> newFilters){
		return userFunctions.updateStackChartFiltersUtil(id,newFilters);
	}
	
	@PostMapping("/updateYTDChart")
	public String updateYTDChart(@RequestParam long id,@RequestParam String Category1, @RequestBody List<Filter> newFilters){
		return userFunctions.updateYTDChartUtil(id,Category1,newFilters);
	}
	
	@PostMapping("/editDashboard")
	public Dashboard editDashboard(@RequestParam long id, @RequestBody Dashboard editedDashboard){
		return userFunctions.editDashboardUtil(id,editedDashboard);
	}
	
	@PostMapping("/deleteDashboard")
	public String deleteDashboard(@RequestParam long id){
		return userFunctions.deleteDashboardUtil(id);
	}
	
}
