package com.db.intern.opt.OPTDashboard.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import com.db.intern.opt.OPTDashboard.OptDashboardApplication;
import com.db.intern.opt.OPTDashboard.model.User;
import com.db.intern.opt.OPTDashboard.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OptDashboardApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
class UserControllerTest {

	@Autowired
	UserFunctions userFunctions;
	
	@Autowired
	UserRepository userRepository;
	
	@LocalServerPort
	private int port;

	TestRestTemplate restTemplate = new TestRestTemplate();

	HttpHeaders headers = new HttpHeaders();
	
	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}
	
	@Test
	@Order(1)
	public void testRegisterController(){
		assertEquals(0,userRepository.count());
		Map<String, String> testUser= new HashMap<>();
		testUser.put("email", "test@db.com");
		testUser.put("name", "test");
		testUser.put("password", "password");
		HttpEntity<Map<String, String>> entity = new HttpEntity<Map<String, String>>(testUser, headers);
		ResponseEntity<String> response = restTemplate.exchange(
				createURLWithPort("/api/users/register"),
				HttpMethod.POST, entity, String.class);
		assertEquals("Account successfully registerd",response.getBody());
		response = restTemplate.exchange(
				createURLWithPort("/api/users/register"),
				HttpMethod.POST, entity, String.class);
		String expected2 = "This Email is already taken!";
		assertEquals(expected2,response.getBody());
		assertEquals(1,userRepository.count());
	}	
	
	
	@Test
	@Order(2)
	public void testLoginController(){
		assertEquals(1,userRepository.count());
		Map<String, String> testUser= new HashMap<>();
		testUser.put("email", "test@db.com");
		testUser.put("password", "wrongpassword");
		HttpEntity<Map<String, String>> entity = new HttpEntity<Map<String, String>>(testUser, headers);
		ResponseEntity<String> response = restTemplate.exchange(
				createURLWithPort("/api/users/login"),
				HttpMethod.POST, entity, String.class);
		assertEquals(null,response.getBody());
		testUser= new HashMap<>();
		testUser.put("email", "test@db.com");
		testUser.put("password", "password");
		entity = new HttpEntity<Map<String, String>>(testUser, headers);
		response = restTemplate.exchange(
				createURLWithPort("/api/users/login"),
				HttpMethod.POST, entity, String.class);
		String expected = "{\"email\":\"test@db.com\",\"name\":\"test\"}";
		assertEquals(expected,response.getBody());
	}	
	
	
	@Test 
	@Order(3) 
	public void testInitializeController(){ 
		assertEquals(1,userRepository.count()); 
		HttpEntity<String> entity = new HttpEntity<String>(null, headers); 
		ResponseEntity<String> response = restTemplate.exchange( 
				createURLWithPort("/api/users/initialize"), 
				HttpMethod.GET, entity, String.class); 
		String expected = "{\"email\":\"test@db.com\",\"name\":\"test\"}"; 
		assertEquals(expected,response.getBody()); 
	}
	
	@Test 
	@Order(4) 
	public void testHasDashboardsControllerBefore(){ 
		HttpEntity<String> entity = new HttpEntity<String>(null, headers); 
		ResponseEntity<String> response = restTemplate.exchange( 
				createURLWithPort("/api/users/hasDashboards"), 
				HttpMethod.GET, entity, String.class);
		assertEquals("false",response.getBody());
	}

	@Test 
	@Order(5) 
	public void testAddDashboardController(){ 
		Map<String, String> Dashbaord1= new HashMap<>();
		Dashbaord1.put("isDefault", "true");
		Dashbaord1.put("name", "Dashbaord1");
		HttpEntity<Map<String, String>> entity = new HttpEntity<Map<String, String>>(Dashbaord1, headers);
		ResponseEntity<String> response = restTemplate.exchange(
				createURLWithPort("/api/users/addDashboard"),
				HttpMethod.POST, entity, String.class);
		String expected = "{\"id\":1,\"name\":\"Dashbaord1\",\"isDefault\":true}"; 
		assertEquals(expected,response.getBody());
		Dashbaord1= new HashMap<>();
		Dashbaord1.put("isDefault", "false");
		Dashbaord1.put("name", "Dashbaord2");
		entity = new HttpEntity<Map<String, String>>(Dashbaord1, headers);
		response = restTemplate.exchange(
				createURLWithPort("/api/users/addDashboard"),
				HttpMethod.POST, entity, String.class);
		expected = "{\"id\":2,\"name\":\"Dashbaord2\",\"isDefault\":false}"; 
		assertEquals(expected,response.getBody());
	}
	
	@Test 
	@Order(6) 
	public void testGetListOfDashboardController(){ 
		HttpEntity<String> entity = new HttpEntity<String>(null, headers); 
		ResponseEntity<String> response = restTemplate.exchange( 
				createURLWithPort("/api/users/getListOfDashboard"), 
				HttpMethod.GET, entity, String.class);
		String expected = "[{\"id\":1,\"name\":\"Dashbaord1\",\"isDefault\":true},{\"id\":2,\"name\":\"Dashbaord2\",\"isDefault\":false}]";
		assertEquals(expected,response.getBody());
	}
	
	@Test 
	@Order(7) 
	public void testHasDashboardsControllerAfter(){ 
		HttpEntity<String> entity = new HttpEntity<String>(null, headers); 
		ResponseEntity<String> response = restTemplate.exchange( 
				createURLWithPort("/api/users/hasDashboards"), 
				HttpMethod.GET, entity, String.class);
		assertEquals("true",response.getBody());
	}
	
	@Test 
	@Order(8) 
	public void testGetDefaultDashboardController(){ 
		HttpEntity<String> entity = new HttpEntity<String>(null, headers); 
		ResponseEntity<String> response = restTemplate.exchange( 
				createURLWithPort("/api/users/getDefaultDashboard"), 
				HttpMethod.GET, entity, String.class);
		String expected = "{\"id\":1,\"name\":\"Dashbaord1\",\"isDefault\":true}";
		assertEquals(expected,response.getBody());
	}
	
	@Test 
	@Order(9) 
	public void testGetDashboardController(){ 
		HttpEntity<String> entity = new HttpEntity<String>(null, headers); 
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort("/api/users/getDashboard"))
		        .queryParam("id", 1);
		ResponseEntity<String> response = restTemplate.exchange( 
				builder.toUriString(), 
				HttpMethod.GET, entity, String.class);
		String expected = "{\"id\":1,\"name\":\"Dashbaord1\",\"isDefault\":true}";
		assertEquals(expected,response.getBody());
		builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort("/api/users/getDashboard"))
		        .queryParam("id", 2);
		response = restTemplate.exchange( 
				builder.toUriString(), 
				HttpMethod.GET, entity, String.class);
		expected = "{\"id\":2,\"name\":\"Dashbaord2\",\"isDefault\":false}";
		assertEquals(expected,response.getBody());
	}
	
	
	@Test 
	@Order(10) 
	public void testAddChartController(){ 
		Map<String, String> Chart1= new HashMap<>();
		Chart1.put("name", "Chart1");
		Chart1.put("type", "Bar Chart");
		Chart1.put("category_1", "UBR Level 2");
		Chart1.put("category_2", "Total");
		Chart1.put("aggregation", "Count");
		HttpEntity<Map<String, String>> entity = new HttpEntity<Map<String, String>>(Chart1, headers);
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort("/api/users/addChart"))
		        .queryParam("id", 1);
		ResponseEntity<String> response = restTemplate.exchange( 
				builder.toUriString(), 
				HttpMethod.POST, entity, String.class);
		assertEquals("Chart added",response.getBody());
		
		Map<String, String> Chart2= new HashMap<>();
		Chart2.put("name", "Chart2");
		Chart2.put("type", "YTD Chart");
		Chart2.put("category_1", "UBR Level 1");
		Chart2.put("category_2", "Position Id Status");
		Chart2.put("aggregation", "Count");
		entity = new HttpEntity<Map<String, String>>(Chart2, headers);
		builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort("/api/users/addChart"))
		        .queryParam("id", 1);
		response = restTemplate.exchange( 
				builder.toUriString(), 
				HttpMethod.POST, entity, String.class);
		assertEquals("Chart added",response.getBody());
		
		Map<String, String> Chart3= new HashMap<>();
		Chart3.put("name", "Chart3");
		Chart3.put("type", "Stacked Bar Chart");
		Chart3.put("category_1", "Location");
		Chart3.put("category_2", "Gender Desc");
		Chart3.put("aggregation", "Count");
		entity = new HttpEntity<Map<String, String>>(Chart3, headers);
		builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort("/api/users/addChart"))
		        .queryParam("id", 2);
		response = restTemplate.exchange( 
				builder.toUriString(), 
				HttpMethod.POST, entity, String.class);
		assertEquals("Chart added",response.getBody());
		
		Map<String, String> Chart4= new HashMap<>();
		Chart4.put("name", "Chart4");
		Chart4.put("type", "Incorrect");
		Chart4.put("category_1", "Incorrect");
		Chart4.put("category_2", "Incorrect Desc");
		Chart4.put("aggregation", "Incorrect");
		entity = new HttpEntity<Map<String, String>>(Chart4, headers);
		builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort("/api/users/addChart"))
		        .queryParam("id", 2);
		response = restTemplate.exchange( 
				builder.toUriString(), 
				HttpMethod.POST, entity, String.class);
		assertEquals(null,response.getBody());
	}
	
	
	
	@Test 
	@Order(11) 
	public void testGetListOfChartsController(){ 
		HttpEntity<String> entity = new HttpEntity<String>(null, headers); 
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort("/api/users/getListOfCharts"))
		        .queryParam("id", 1);
		ResponseEntity<String> response = restTemplate.exchange( 
				builder.toUriString(), 
				HttpMethod.GET, entity, String.class);
		String expected = "[{\"id\":1,\"name\":\"Chart1\",\"type\":\"Bar Chart\",\"category_1\":\"UBR Level 2\",\"category_2\":\"Total\",\"aggregation\":\"Count\"},{\"id\":2,\"name\":\"Chart2\",\"type\":\"YTD Chart\",\"category_1\":\"UBR Level 1\",\"category_2\":\"Position Id Status\",\"aggregation\":\"Count\"}]";
		assertEquals(expected,response.getBody());
		builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort("/api/users/getListOfCharts"))
		        .queryParam("id", 2);
		response = restTemplate.exchange( 
				builder.toUriString(), 
				HttpMethod.GET, entity, String.class);
		expected = "[{\"id\":3,\"name\":\"Chart3\",\"type\":\"Stacked Bar Chart\",\"category_1\":\"Location\",\"category_2\":\"Gender Desc\",\"aggregation\":\"Count\"}]";
		assertEquals(expected,response.getBody());
	}
	
	
	@Test 
	@Order(12) 
	public void testGetChartFiltersController(){ 
		HttpEntity<String> entity = new HttpEntity<String>(null, headers); 
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort("/api/users/getChartFilters"))
		        .queryParam("id", 2);
		ResponseEntity<String> response = restTemplate.exchange( 
				builder.toUriString(), 
				HttpMethod.GET, entity, String.class);
		String expected = "[\"Position Id Status\",\"2021\"]";
		assertEquals(expected,response.getBody());
		
		builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort("/api/users/getChartFilters"))
		        .queryParam("id", 3);
		response = restTemplate.exchange( 
				builder.toUriString(), 
				HttpMethod.GET, entity, String.class);
		expected = "[\"Female\",\"Bangalore\",\"Male\",\"Pune\"]";
		assertEquals(expected,response.getBody());
	}
	
	@Test 
	@Order(13) 
	public void testEditDashboardController(){ 
		Map<String, String> Dashbaord1= new HashMap<>();
		Dashbaord1.put("isDefault", "true");
		Dashbaord1.put("name", "Dashbaord1edited");
		HttpEntity<Map<String, String>> entity = new HttpEntity<Map<String, String>>(Dashbaord1, headers);
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort("/api/users/editDashboard"))
		        .queryParam("id", 1);
				
		ResponseEntity<String> response = restTemplate.exchange(
				builder.toUriString(), 
				HttpMethod.POST, entity, String.class);
		String expected = "{\"id\":1,\"name\":\"Dashbaord1edited\",\"isDefault\":true}";
		assertEquals(expected,response.getBody());
	}
	
	
	@Test 
	@Order(13) 
	public void testDeleteDashboardController(){ 
		HttpEntity<String> entity = new HttpEntity<String>(null, headers); 
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort("/api/users/deleteDashboard"))
		        .queryParam("id", 2);
		ResponseEntity<String> response = restTemplate.exchange(
				builder.toUriString(), 
				HttpMethod.POST, entity, String.class);
		assertEquals("Dashbaord deleted successfully",response.getBody());
	}
	
	
	@Test 
	@Order(14) 
	public void testGetListOfDashboardControllerAfterDelete(){ 
		HttpEntity<String> entity = new HttpEntity<String>(null, headers); 
		ResponseEntity<String> response = restTemplate.exchange( 
				createURLWithPort("/api/users/getListOfDashboard"), 
				HttpMethod.GET, entity, String.class);
		String expected = "[{\"id\":1,\"name\":\"Dashbaord1edited\",\"isDefault\":true}]";
		assertEquals(expected,response.getBody());
	}

	@Test 
	@Order(15) 
	public void testLogoutController(){ 
		HttpEntity<String> entity = new HttpEntity<String>(null, headers); 
		ResponseEntity<String> response = restTemplate.exchange( 
				createURLWithPort("/api/users/logout"), 
				HttpMethod.POST, entity, String.class);
		assertEquals("Logout successfull",response.getBody());
	}
	
	
	@Test 
	@Order(16) 
	public void testInitializeControllerAfterLogout(){ 
		HttpEntity<String> entity = new HttpEntity<String>(null, headers); 
		ResponseEntity<String> response = restTemplate.exchange( 
				createURLWithPort("/api/users/initialize"), 
				HttpMethod.GET, entity, String.class); 
		assertEquals(null,response.getBody()); 
	}

	
}
