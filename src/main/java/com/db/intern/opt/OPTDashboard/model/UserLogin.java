package com.db.intern.opt.OPTDashboard.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalTime;

@Entity
@Table(name = "user_logins")
public class UserLogin {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
//	private int userId;
	private LocalTime loginTime;
	private LocalTime logoutTime;
	
	public UserLogin() {
	}

	public UserLogin(LocalTime loginTime, LocalTime logoutTime) {
		super();
		this.loginTime = loginTime;
		this.logoutTime = logoutTime;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalTime getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(LocalTime loginTime) {
		this.loginTime = loginTime;
	}

	public LocalTime getLogoutTime() {
		return logoutTime;
	}

	public void setLogoutTime(LocalTime logoutTime) {
		this.logoutTime = logoutTime;
	}

	@Override
	public String toString() {
		return "UserLogin [id=" + id + ", loginTime=" + loginTime + ", logoutTime=" + logoutTime + "]";
	}
	
	
}