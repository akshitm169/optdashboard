package com.db.intern.opt.OPTDashboard.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.db.intern.opt.OPTDashboard.model.UserLogin;

@Repository
public interface UserLoginRepository extends JpaRepository<UserLogin, Long>{
	
	//List<UserLogin> findByUser_id(Integer user_id);
//	List<UserLogin> findByUserId(Integer id);
	
}