package com.db.intern.opt.OPTDashboard.controller;


import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.db.intern.opt.OPTDashboard.model.JobData;

import com.db.intern.opt.OPTDashboard.controller.CacheManager.Cache;

@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping("/api/v1/")
public class FirstController {
	
	@GetMapping("/")
	public String home(){
		return "Omg Hi!";
	}
	@GetMapping("/all")
	public List<JobData> getAll(){
		return CacheManager.getCache(Cache.JOB_DATA);
	}
	@GetMapping("/chartdata1")
	public ConcurrentHashMap<String,Integer> getChartData1(){
		return ChartData.getBarChartData("positionIDStatus");
	}
	@GetMapping("/bannerdata")
	public ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> bannerdata(){
		return ChartData.getBannerData();
	}
	@GetMapping("/chartdata2")
	public ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> getChartData2(@RequestParam String category1, @RequestParam String category2){
		return ChartData.getStackedBarChartData(category1,category2);
	}
	
	@GetMapping("/filter")
	public ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> filter(@RequestParam String category1, @RequestParam String category2,@RequestParam List<String> values){
		return ChartData.filter(category1,category2,values);
	}
	
	@GetMapping("/drill")
	public String drill(@RequestParam String label, @RequestParam boolean pivot){
		System.out.println(label);
		Category labelEnum = Category.getEnumByLabel(label); 
		if(pivot) {
			return DrillLevels.drillUp(labelEnum,labelEnum.getGroup());
		}
		else {
			return DrillLevels.drillDown(labelEnum,labelEnum.getGroup());
		}
	}
	
	@GetMapping("/YTD")
	public ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> YTD(@RequestParam int reqYear, @RequestParam String reqCategory, @RequestParam String reqPositionIDStatus){
		return YTDFunctions.getYTDData(reqYear,reqCategory,reqPositionIDStatus);
	}
	
	@GetMapping("/Years")
	public int Years(){
		return YTDFunctions.getYears();
	}
	
	
}
