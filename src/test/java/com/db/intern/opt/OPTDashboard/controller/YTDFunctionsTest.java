package com.db.intern.opt.OPTDashboard.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.jupiter.api.Test;

import com.db.intern.opt.OPTDashboard.controller.CacheManager.Cache;
import com.db.intern.opt.OPTDashboard.model.JobData;

class YTDFunctionsTest {

	private static final String CSV_PATH = "src/main/resources/JobData.csv";

	@Test
	void init() {
		List<JobData> jobDataList= CSVReader.readDataFromCSV(CSV_PATH);
		CacheManager.setCache(Cache.JOB_DATA,jobDataList);
	}
	
	@Test
	void testgetYTDData() {
		ConcurrentHashMap<String, ConcurrentHashMap<String, Integer>> result = YTDFunctions.getYTDData(2021,"UBR Level 2","On Hold");
		assertEquals(12,result.size()); 
		 List<String> months = Arrays.asList("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"); 
		  for(String month: months) { 
			   assertTrue(result.containsKey(month)); 
		  }
	}
	
	@Test
	void testMinYear() {
		int result = YTDFunctions.getYears();
		assertEquals(2020,result); 
	}

}
