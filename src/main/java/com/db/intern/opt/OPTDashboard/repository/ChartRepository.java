package com.db.intern.opt.OPTDashboard.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.db.intern.opt.OPTDashboard.model.Chart;

@Repository
public interface ChartRepository extends JpaRepository<Chart, Long>{

}
