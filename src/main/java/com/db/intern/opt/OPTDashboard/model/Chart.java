package com.db.intern.opt.OPTDashboard.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name = "charts")
public class Chart {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
//	private Integer userId;
	private String name;
	private String type;
	private String category_1;
	private String category_2;
	private String aggregation;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn( name = "chart_id", referencedColumnName = "id")
	List<Filter> chartFilters = new ArrayList<>();
	
	public Chart() {
	}
	
	public Chart(String name, String type, String category_1, String category_2,
			String aggregation) {
		super();
		this.name = name;
		this.type = type;
		this.category_1 = category_1;
		this.category_2 = category_2;
		this.aggregation = aggregation;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCategory_1() {
		return category_1;
	}
	public void setCategory_1(String category_1) {
		this.category_1 = category_1;
	}
	public String getCategory_2() {
		return category_2;
	}
	public void setCategory_2(String category_2) {
		this.category_2 = category_2;
	}
	public String getAggregation() {
		return aggregation;
	}
	public void setAggregation(String aggregation) {
		this.aggregation = aggregation;
	}
	
	@JsonProperty(access = Access.WRITE_ONLY)
	public List<Filter> getChartFilters() {
		return chartFilters;
	}

	public void setChartFilters(List<Filter> chartFilters) {
		this.chartFilters.clear();
		if(chartFilters.size()!=0) {
			this.chartFilters.addAll(chartFilters);
		}
	}

	@Override
	public String toString() {
		return "Chart [id=" + id + ", name=" + name + ", type=" + type + ", category_1=" + category_1 + ", category_2="
				+ category_2 + ", aggregation=" + aggregation + "]";
	}

	
	
	
}
