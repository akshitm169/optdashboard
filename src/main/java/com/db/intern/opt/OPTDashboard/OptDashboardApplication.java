package com.db.intern.opt.OPTDashboard;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class OptDashboardApplication  {

	public static void main(String[] args) {
		SpringApplication.run(OptDashboardApplication.class, args);
	}

}
