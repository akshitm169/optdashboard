package com.db.intern.opt.OPTDashboard.controller;

import com.db.intern.opt.OPTDashboard.model.JobData;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class CacheManager {

	enum Cache{
		JOB_DATA;
	}
	
	private static ConcurrentHashMap<Cache, List<JobData>> cacheHashMap = new ConcurrentHashMap<Cache, List<JobData>>();
	
	public static List<JobData> getCache(Cache cache){
		return cacheHashMap.get(cache);
	}
	
	public static void setCache(Cache cache, List<JobData> jobDataList) {
		cacheHashMap.put(cache, jobDataList);
	}
}
