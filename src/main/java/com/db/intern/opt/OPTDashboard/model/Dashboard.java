package com.db.intern.opt.OPTDashboard.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name = "dashboards")
public class Dashboard {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	private Boolean isDefault;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn( name = "dashboard_id", referencedColumnName = "id")
	List<Chart> dashboardsCharts = new ArrayList<>();
	
	public Dashboard() {
	}
	
	
	public Dashboard(String name,Boolean isDefault) {
		super();
		this.name = name;
		this.isDefault = isDefault;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}

//	@JsonProperty(access = Access.WRITE_ONLY)
	public Boolean getIsDefault() {
		return isDefault;
	}


	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	@JsonProperty(access = Access.WRITE_ONLY)
	public List<Chart> getDashboardsCharts() {
		return dashboardsCharts;
	}


	public void setDashboardsCharts(List<Chart> dashboardsCharts) {
		this.dashboardsCharts = dashboardsCharts;
	}


	@Override
	public String toString() {
		return "Dashboard [id=" + id + ", name=" + name + ", isDefault=" + isDefault + ", dashboardsCharts="
				+ dashboardsCharts + "]";
	}
	
	
	
	
}
