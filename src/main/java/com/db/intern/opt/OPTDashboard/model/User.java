package com.db.intern.opt.OPTDashboard.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name = "users")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String email;
	private String name;
	
	private String password;
//	private Boolean isLoggedIn=false;
	private Boolean isLoggedIn;

	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn( name = "user_id", referencedColumnName = "id")
	List<UserLogin> userlogins = new ArrayList<>();
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn( name = "user_id", referencedColumnName = "id")
	List<Dashboard> userDashboards = new ArrayList<>();

	
	
	public User() {
	}
	public User(String email, String name, String password) {
		super();
		this.email = email;
		this.name = name;
		this.password = password;
	}
	
	@JsonProperty(access = Access.WRITE_ONLY)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@JsonProperty(access = Access.WRITE_ONLY)
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@JsonProperty(access = Access.WRITE_ONLY)
	public Boolean getIsLoggedIn() {
		return isLoggedIn;
	}
	public void setIsLoggedIn(Boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}
	
	@JsonProperty(access = Access.WRITE_ONLY)
	public List<UserLogin> getUserlogins() {
		return userlogins;
	}
	public void setUserlogins(List<UserLogin> userlogins) {
		this.userlogins = userlogins;
	}
	
	@JsonProperty(access = Access.WRITE_ONLY)
	public List<Dashboard> getUserDashboards() {
		return userDashboards;
	}
	public void setUserDashboards(List<Dashboard> userDashboards) {
		this.userDashboards = userDashboards;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", email=" + email + ", name=" + name + ", password=" + password + ", isLoggedIn="
				+ isLoggedIn + ", userDashboards=" + userDashboards + "]";
	}
	
	
	
}