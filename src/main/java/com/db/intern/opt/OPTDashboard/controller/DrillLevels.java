package com.db.intern.opt.OPTDashboard.controller;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;


@Component
public class DrillLevels {
	
	@PostConstruct
	public static void init() {
		Category.setLevels();
	}
	
	public static String drillDown(Category cat,String groupName){
		List<Category> categoryLevel = Category.GROUPS.get(groupName);
		int pos = Math.min(categoryLevel.indexOf(cat)+1,categoryLevel.size()-1);
		return categoryLevel.get(pos).getLabel();
	}
	
	public static String drillUp(Category cat,String groupName){
		List<Category> categoryLevel = Category.GROUPS.get(groupName);
		int pos = Math.max(categoryLevel.indexOf(cat)-1,0);
		return categoryLevel.get(pos).getLabel();
	}
	
}
