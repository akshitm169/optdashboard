package com.db.intern.opt.OPTDashboard.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.db.intern.opt.OPTDashboard.model.Chart;
import com.db.intern.opt.OPTDashboard.model.Dashboard;
import com.db.intern.opt.OPTDashboard.model.Filter;
import com.db.intern.opt.OPTDashboard.model.User;
import com.db.intern.opt.OPTDashboard.model.UserLogin;
import com.db.intern.opt.OPTDashboard.repository.ChartRepository;
import com.db.intern.opt.OPTDashboard.repository.DashboardRepository;
import com.db.intern.opt.OPTDashboard.repository.FilterRepository;
import com.db.intern.opt.OPTDashboard.repository.UserLoginRepository;
import com.db.intern.opt.OPTDashboard.repository.UserRepository;

import javafx.util.Pair;

import java.time.LocalTime;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.UnaryOperator;


@Service
public class UserFunctions {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	UserLoginRepository userLoginRepository;
	
	@Autowired
	DashboardRepository dashboardRepository;
	
	@Autowired
	ChartRepository chartRepository;
	
	@Autowired
	FilterRepository filterRepository;

	public String registerUtil(User user) {
//		System.out.println(user);
//		System.out.println(user.getEmail());
//		System.out.println(userRepository.count());
		if(userRepository.findByemail(user.getEmail())!=null) return "This Email is already taken!";
		userRepository.save(user);
		return "Account successfully registerd";
	}
	
	public User loginUtil(User user) {
		User checkUser = userRepository.findByemail(user.getEmail());
//		System.out.println(checkUser);
		if(checkUser == null || !user.getPassword().equals(checkUser.getPassword())) {
			return null;
		}
		checkUser.setIsLoggedIn(true);
		checkUser.getUserlogins().add(new UserLogin(LocalTime.now(),null));
		userRepository.save(checkUser);
		return checkUser;
		
	}
	
	public String logoutUtil() {
		User activeUser = userRepository.findByisLoggedInTrue();
		activeUser.setIsLoggedIn(false);
		int l = activeUser.getUserlogins().size();
		activeUser.getUserlogins().get(l-1).setLogoutTime(LocalTime.now());
		userRepository.save(activeUser);	
		return "Logout successfull";
	}
	
	public User initalizeUtil() {
		User activeUser = userRepository.findByisLoggedInTrue();
		return activeUser;
	}
	
	
	public Dashboard addDashboardUtil(Dashboard dashboard) {
		User activeUser = userRepository.findByisLoggedInTrue();
		if(dashboard.getIsDefault()) {
			for(Dashboard d: activeUser.getUserDashboards()) {
				d.setIsDefault(false);
			}
		}
		activeUser.getUserDashboards().add(dashboard);
		userRepository.save(activeUser);	
		int l = activeUser.getUserDashboards().size();
		return activeUser.getUserDashboards().get(l-1);
	}
	
	public Dashboard editDashboardUtil(long id, Dashboard editedDashboard) {
		User activeUser = userRepository.findByisLoggedInTrue();
		if(editedDashboard.getIsDefault()) {
			for(Dashboard d: activeUser.getUserDashboards()) {
//				System.out.println(d);
				d.setIsDefault(false);
//				dashboardRepository.save(d);
			}
		}
		Dashboard currDashboard = dashboardRepository.findById(id).orElse(null);
		currDashboard.setName(editedDashboard.getName());
		currDashboard.setIsDefault(editedDashboard.getIsDefault());
		dashboardRepository.save(currDashboard);
		return currDashboard;
	}
	
	public String deleteDashboardUtil(long id) {
		dashboardRepository.deleteById(id);
		return "Dashbaord deleted successfully";
	}
	
	public String addStackedChartUtil(long id,Chart chart){
		Dashboard currDashboard = dashboardRepository.findById(id).orElse(null);
//		chart.getChartFilters().add(new Filter("UBR Level 2","Level_2_Banking"));
		Set<Pair<String,String>> stackedBarChartDataFilters =ChartData.getStackedBarChartDataFilters(chart.getCategory_1(),chart.getCategory_2());
		for(Pair<String,String> p: stackedBarChartDataFilters) {
			chart.getChartFilters().add(new Filter(p.getKey(),p.getValue()));
		}
		currDashboard.getDashboardsCharts().add(chart);
		dashboardRepository.save(currDashboard);
		return "Chart added";
	}
	
	public String addYTDChartUtil(long id,Chart chart){
		Dashboard currDashboard = dashboardRepository.findById(id).orElse(null);
		chart.getChartFilters().add(new Filter("Position Id Status",chart.getCategory_2()));
		chart.getChartFilters().add(new Filter("Year",String.valueOf(Year.now().getValue())));
		chart.setCategory_2("Position Id Status");
		currDashboard.getDashboardsCharts().add(chart);
		dashboardRepository.save(currDashboard);
		return "Chart added";
	}
	
	
	
	public List<Dashboard> getListOfDashboardUtil(){
		if(userRepository.findByisLoggedInTrue()!=null) return userRepository.findByisLoggedInTrue().getUserDashboards();
		else return null;
	}
	
	public List<Chart> getListOfChartsUtil(long id){
		return dashboardRepository.findById(id).orElse(null).getDashboardsCharts();
	}
	
	public Dashboard getDefaultDashboardUtil(){
		List<Dashboard> allDashbaords = userRepository.findByisLoggedInTrue().getUserDashboards();
		for(Dashboard d:allDashbaords) {
			if(d.getIsDefault()){return d;}
		}
		return null;
	}
	public Dashboard getDashboardUtil(long id){
		return dashboardRepository.findById(id).orElse(null);
	}
	public List<String> getChartFiltersUtil(long id) {
		List<Filter> currChartFilters = chartRepository.findById(id).orElse(null).getChartFilters();
		List<String> filtervals = new ArrayList<String>();
		for(Filter f: currChartFilters) {
			filtervals.add(f.getName());
		}
		return filtervals;
	}
	
	public String updateStackChartCategory1Util(long id,String Category1) {
		Chart currChart = chartRepository.findById(id).orElse(null);
		currChart.setCategory_1(Category1);
		chartRepository.save(currChart);
		return "Category1 Successfully Updated";
	}
	
	public String updateStackChartFiltersUtil(long id, List<Filter> newFilters) {
		Chart currChart = chartRepository.findById(id).orElse(null);
		currChart.setChartFilters(newFilters);
		chartRepository.save(currChart);
		return "Filters Successfully Updated";
	}
	public String updateYTDChartUtil(long id,String Category1,List<Filter> newFilters){
		Chart currChart = chartRepository.findById(id).orElse(null);
		currChart.setCategory_1(Category1);
		System.out.println(newFilters);
		currChart.setChartFilters(newFilters);
		chartRepository.save(currChart);
		return "Filters Successfully Updated";
	}
	
}
