package com.db.intern.opt.OPTDashboard.controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.db.intern.opt.OPTDashboard.controller.CacheManager.Cache;
import com.db.intern.opt.OPTDashboard.model.JobData;

@Component
public class CSVReader {
	
	private static final String CSV_PATH = "src/main/resources/JobData.csv";
	
	@PostConstruct
	public void init() {
		List<JobData> jobDataList= this.readDataFromCSV(CSV_PATH);
		CacheManager.setCache(Cache.JOB_DATA,jobDataList);
	}

	public static List<JobData> readDataFromCSV(String filePath){
        List<JobData> data = new ArrayList<>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(filePath));
            String line = br.readLine();
            while((line = br.readLine()) != null) {
                String[] attributes = line.split(",");
                JobData entry = createEntry(attributes);
                data.add(entry);
            }
            
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}

	private static JobData createEntry(String[] attributes) {
		
		String jobRequisitionID = attributes[0];
		String jobOpeningReasonDesc = attributes[1];
		String ubrLevel2 = attributes[2];
		String ubrLevel1 = attributes[3];
		String subBU = attributes[4];
		String countryDesc = attributes[5];
		String location = attributes[6];
		String title = attributes[7];
		String hiringManagerName = attributes[8];
		LocalDate requsitionRaisedDate = LocalDate.parse(attributes[9], DateTimeFormatter.ofPattern("d-MMM-yy"));
		LocalDate jobPostingStartDate = LocalDate.parse(attributes[10], DateTimeFormatter.ofPattern("d-MMM-yy"));
		LocalDate jobPostingEndDate = LocalDate.parse(attributes[11], DateTimeFormatter.ofPattern("d-MMM-yy"));
		String candidateID = attributes[12];
		LocalDate offerDate = LocalDate.parse(attributes[13], DateTimeFormatter.ofPattern("d-MMM-yy"));
		String genderDesc = attributes[14];
		String pwd = attributes[15];
		String positionIDStatus = attributes[16];
		String skills = attributes[17];
		return new JobData(jobRequisitionID, jobOpeningReasonDesc, ubrLevel2, ubrLevel1,
				subBU,countryDesc, location, title, hiringManagerName,
				requsitionRaisedDate, jobPostingStartDate, jobPostingEndDate,
				candidateID, offerDate, genderDesc, pwd, positionIDStatus,
				skills);
	}
	
	
}
