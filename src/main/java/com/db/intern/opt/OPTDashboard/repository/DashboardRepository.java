package com.db.intern.opt.OPTDashboard.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.db.intern.opt.OPTDashboard.model.Dashboard;

@Repository
public interface DashboardRepository extends JpaRepository<Dashboard,Long> {
	

}
