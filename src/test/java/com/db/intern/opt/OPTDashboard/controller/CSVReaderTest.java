package com.db.intern.opt.OPTDashboard.controller;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.db.intern.opt.OPTDashboard.model.JobData;
import com.google.gson.Gson;

class CSVReaderTest {

	CSVReader csvreader = new CSVReader();

	@Test
	void testListEntriesCount() {
		List<JobData> result = csvreader.readDataFromCSV("src/main/resources/JobData.csv");
		int rows = 161;
		assertEquals(rows,result.size());
	}
	
	@Test
	void testList() {
		List<JobData> result = csvreader.readDataFromCSV("src/main/resources/dummy.csv");
        List<JobData> expected = new ArrayList<>();
        expected.add(new JobData("JR0072366", "Permanent: Replacement > Replacement", "Level_2_Banking", "Level_1_Corporate_Functions",
        		"Compliance","India", "Pune", "Associate", "HiringManager102",
        		 LocalDate.parse("07-Jan-20",DateTimeFormatter.ofPattern("d-MMM-yy")), LocalDate.parse("01-Mar-20",DateTimeFormatter.ofPattern("d-MMM-yy")),
        		 LocalDate.parse("24-Apr-20",DateTimeFormatter.ofPattern("d-MMM-yy")),"CAD00000069176",
        		 LocalDate.parse("16-Sep-20",DateTimeFormatter.ofPattern("d-MMM-yy")), "Female", "FALSE", "On Hold",
				"Fullstack + Angular "));
		assertEquals(expected.size(),result.size());
		String expectedString = new Gson().toJson(expected);
    	String resultString = new Gson().toJson(result); 
        assertEquals(expectedString,resultString);
	}

}
