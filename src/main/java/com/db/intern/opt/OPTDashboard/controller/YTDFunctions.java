package com.db.intern.opt.OPTDashboard.controller;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.time.LocalDate;
import java.time.Year;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.Set;

import com.db.intern.opt.OPTDashboard.controller.CacheManager.Cache;
import com.db.intern.opt.OPTDashboard.model.JobData;

public class YTDFunctions {
	
	public static ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>> getYTDData(int reqYear, String reqCategory, String reqPositionIDStatus){
		List<String> months = Arrays.asList("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
		ConcurrentHashMap<String, ConcurrentHashMap<String, Integer>> YTDData= new ConcurrentHashMap<String, ConcurrentHashMap<String,Integer>>();
		for(String monthiter: months) {
			YTDData.put(monthiter, new ConcurrentHashMap<String, Integer>());
		}
		List<JobData> jobDataList = CacheManager.getCache(Cache.JOB_DATA);
		for(JobData row:jobDataList) {
			LocalDate offerDate = row.getOfferDate();
			String PositionIDStatus = row.getPositionIDStatus();
			if((reqYear != offerDate.getYear()) || !(reqPositionIDStatus.equalsIgnoreCase(PositionIDStatus) )) continue;
			String offerMonth = offerDate.getMonth().getDisplayName(TextStyle.SHORT,Locale.ENGLISH);
			String categoryType=row.getAttribute(reqCategory);
			if (YTDData.containsKey(offerMonth)){
				
				int count = YTDData.get(offerMonth).containsKey(categoryType) ? YTDData.get(offerMonth).get(categoryType) : 0;
				YTDData.get(offerMonth).put(categoryType, count + 1);
				
	         }else{
	        	 ConcurrentHashMap<String,Integer> InnerMap = new ConcurrentHashMap<String, Integer>();
	        	 InnerMap.put(categoryType,1);
	        	 YTDData.put(offerMonth,InnerMap);
	         }
		}
		return YTDData;
	}
	
	public static int getYears() {
		List<JobData> jobDataList = CacheManager.getCache(Cache.JOB_DATA);
		int minYear = Year.now().getValue();
		for(JobData row:jobDataList) {
			minYear = Math.min(minYear,row.getOfferDate().getYear());
		}
		return minYear;
	}
	
	
	
	
}
