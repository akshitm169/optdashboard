package com.db.intern.opt.OPTDashboard.model;

import java.time.LocalDate;

public class JobData {
	private String jobRequisitionID;
	private String jobOpeningReasonDesc;
	private String ubrLevel2;
	private String ubrLevel1;
	private String subBU;
	private String countryDesc;
	private String location;
	private String title;
	private String hiringManagerName;
	private LocalDate requsitionRaisedDate;
	private LocalDate jobPostingStartDate;
	private LocalDate jobPostingEndDate;
	private String candidateID;
	private LocalDate offerDate;
	private String genderDesc;
	private String pwd;
	private String positionIDStatus;
	private String skills;

	
	public JobData(String jobRequisitionID, String jobOpeningReasonDesc, String ubrLevel2, String ubrLevel1,
			String subBU, String countryDesc, String location, String title, String hiringManagerName,
			LocalDate requsitionRaisedDate, LocalDate jobPostingStartDate, LocalDate jobPostingEndDate,
			String candidateID, LocalDate offerDate, String genderDesc, String pwd, String positionIDStatus,
			String skills) {
		super();
		this.jobRequisitionID = jobRequisitionID;
		this.jobOpeningReasonDesc = jobOpeningReasonDesc;
		this.ubrLevel2 = ubrLevel2;
		this.ubrLevel1 = ubrLevel1;
		this.subBU = subBU;
		this.countryDesc = countryDesc;
		this.location = location;
		this.title = title;
		this.hiringManagerName = hiringManagerName;
		this.requsitionRaisedDate = requsitionRaisedDate;
		this.jobPostingStartDate = jobPostingStartDate;
		this.jobPostingEndDate = jobPostingEndDate;
		this.candidateID = candidateID;
		this.offerDate = offerDate;
		this.genderDesc = genderDesc;
		this.pwd = pwd;
		this.positionIDStatus = positionIDStatus;
		this.skills = skills;
	}
	
	public String getAttribute(String attr) {
		
		attr = attr.replaceAll("\\s", "").toLowerCase();
		
		switch(attr) {
			case "jobrequisitionid":
				return this.getJobRequisitionID();
			
			case "jobopeningreasondesc":
				return this.getJobOpeningReasonDesc();
				
			case "ubrlevel2":
				return this.getUbrLevel2();
			
			case "ubrlevel1":
				return this.getUbrLevel1();
			
			case "subbu":
				return this.getSubBU();
			
			case "countrydesc":
				return this.getCountryDesc();
			
			case "location":
				return this.getLocation();
			
			case "title":
				return this.getTitle();
			
			case "hiringmanagername":
				return this.getHiringManagerName();
			
			case "candidateid":
				return this.getCandidateID();
			
			case "genderdesc":
				return this.getGenderDesc();
			
			case "pwd":
				return this.getPwd();
			
			case "positionidstatus":
				return this.getPositionIDStatus();
			
			case "skills":
				return this.getSkills();
			
			case "requsitionraiseddate":
				return this.getRequsitionRaisedDate().toString();
			
			case "jobpostingstartdate":
				return this.getJobPostingStartDate().toString();
			
			case "jobpostingenddate":
				return this.getJobPostingEndDate().toString();
			
			case "offerdate":
				return this.getOfferDate().toString();
				
			default:
				return "Total";
		}
	}
	
	
	public String getJobRequisitionID() {
		return jobRequisitionID;
	}
	public void setJobRequisitionID(String jobRequisitionID) {
		this.jobRequisitionID = jobRequisitionID;
	}
	public String getJobOpeningReasonDesc() {
		return jobOpeningReasonDesc;
	}
	public void setJobOpeningReasonDesc(String jobOpeningReasonDesc) {
		this.jobOpeningReasonDesc = jobOpeningReasonDesc;
	}
	public String getUbrLevel2() {
		return ubrLevel2;
	}
	public void setUbrLevel2(String ubrLevel2) {
		this.ubrLevel2 = ubrLevel2;
	}
	public String getUbrLevel1() {
		return ubrLevel1;
	}
	public void setUbrLevel1(String ubrLevel1) {
		this.ubrLevel1 = ubrLevel1;
	}
	public String getSubBU() {
		return subBU;
	}
	public void setSubBU(String subBU) {
		this.subBU = subBU;
	}
	public String getCountryDesc() {
		return countryDesc;
	}
	public void setCountryDesc(String countryDesc) {
		this.countryDesc = countryDesc;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getHiringManagerName() {
		return hiringManagerName;
	}
	public void setHiringManagerName(String hiringManagerName) {
		this.hiringManagerName = hiringManagerName;
	}
	public LocalDate getRequsitionRaisedDate() {
		return requsitionRaisedDate;
	}
	public void setRequsitionRaisedDate(LocalDate requsitionRaisedDate) {
		this.requsitionRaisedDate = requsitionRaisedDate;
	}
	public LocalDate getJobPostingStartDate() {
		return jobPostingStartDate;
	}
	public void setJobPostingStartDate(LocalDate jobPostingStartDate) {
		this.jobPostingStartDate = jobPostingStartDate;
	}
	public LocalDate getJobPostingEndDate() {
		return jobPostingEndDate;
	}
	public void setJobPostingEndDate(LocalDate jobPostingEndDate) {
		this.jobPostingEndDate = jobPostingEndDate;
	}
	public String getCandidateID() {
		return candidateID;
	}
	public void setCandidateID(String candidateID) {
		this.candidateID = candidateID;
	}
	public LocalDate getOfferDate() {
		return offerDate;
	}
	public void setOfferDate(LocalDate offerDate) {
		this.offerDate = offerDate;
	}
	public String getGenderDesc() {
		return genderDesc;
	}
	public void setGenderDesc(String genderDesc) {
		this.genderDesc = genderDesc;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getPositionIDStatus() {
		return positionIDStatus;
	}
	public void setPositionIDStatus(String positionIDStatus) {
		this.positionIDStatus = positionIDStatus;
	}
	public String getSkills() {
		return skills;
	}
	public void setSkills(String skills) {
		this.skills = skills;
	}

	@Override
	public String toString() {
		return "JobData [jobRequisitionID=" + jobRequisitionID + ", jobOpeningReasonDesc=" + jobOpeningReasonDesc
				+ ", ubrLevel2=" + ubrLevel2 + ", ubrLevel1=" + ubrLevel1 + ", subBU=" + subBU + ", countryDesc="
				+ countryDesc + ", location=" + location + ", title=" + title + ", hiringManagerName="
				+ hiringManagerName + ", requsitionRaisedDate=" + requsitionRaisedDate + ", jobPostingStartDate="
				+ jobPostingStartDate + ", jobPostingEndDate=" + jobPostingEndDate + ", candidateID=" + candidateID
				+ ", offerDate=" + offerDate + ", genderDesc=" + genderDesc + ", pwd=" + pwd + ", positionIDStatus="
				+ positionIDStatus + ", skills=" + skills + "]";
	}

	
}
