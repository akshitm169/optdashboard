package com.db.intern.opt.OPTDashboard.controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

class DrillLevelsTest {

	@Test
	void testDrillDown() {
		DrillLevels.init();
		String result;
		Category labelEnum;
		labelEnum  = Category.getEnumByLabel("UBR Level 2"); 
		result = DrillLevels.drillDown(labelEnum, labelEnum.getGroup());
		assertEquals("UBR Level 1",result);
		labelEnum  = Category.getEnumByLabel("UBR Level 1"); 
		result = DrillLevels.drillDown(labelEnum, labelEnum.getGroup());
		assertEquals("Sub BU",result);
//		labelEnum  = Category.getEnumByLabel("Sub BU"); 
//		assertEquals("Sub BU",labelEnum,"crap");
//		result = DrillLevels.drillDown(labelEnum, labelEnum.getGroup());
//		assertEquals("UBR Level 2",result,result);
	}
	
	@Test
	void testDrillUp() {
		DrillLevels.init();
		String result;
		Category labelEnum;
		labelEnum  = Category.getEnumByLabel("UBR Level 2"); 
		result = DrillLevels.drillUp(labelEnum, labelEnum.getGroup());
		assertEquals("UBR Level 2",result);
		labelEnum  = Category.getEnumByLabel("UBR Level 1"); 
		result = DrillLevels.drillUp(labelEnum, labelEnum.getGroup());
		assertEquals("UBR Level 2",result);
		labelEnum  = Category.getEnumByLabel("Sub BU"); 
		result = DrillLevels.drillUp(labelEnum, labelEnum.getGroup());
		assertEquals("UBR Level 1",result);
	}
	
	
	

}
